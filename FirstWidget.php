<?php


class FirstWidget extends WP_Widget{

    public function __construct()
    {
        parent::__construct('first_widget', 'First Widget', [
            'description' => 'Un widget tout nul'
        ]);
    }

    public function widget($args, $instance)
    {
        ?>
            <div class="first-widget">
                <p><?php echo $instance['content'];  ?></p>
            </div>

        <?php
    }

    public function form($instance)
    {

        ?>
            <label for="<?php echo $this->get_field_id('content') ?>">Contenu</label>
            <textarea id="<?php echo $this->get_field_id('content') ?>" name="<?php echo $this->get_field_name('content') ?>"><?php echo $instance['content']; ?></textarea>

        <?php
    }

    public function update($new_instance, $old_instance)
    {
        $instance = [];

        $instance['content'] = $new_instance['content'];

        return $instance;
        
    }
}