<?php
/**
 * Plugin Name: Simplon Plugin
 * Description: Un plugin d'exemple fait à simplon
 */

 
require __DIR__.'/../timber-library/lib/Timber.php';

Timber\Timber::$dirname = ['templates', 'views'];

add_action('wp_enqueue_scripts', 'widget_assets');

//ici on fait une fonction qui va charger le fichier style.css du theme parent
function widget_assets() {
    wp_enqueue_script('modal-script', plugins_url('assets/modal.js', __FILE__), [], false, true);
    wp_enqueue_style('modal-style', plugins_url('assets/modal.css', __FILE__));

}

//hook pour gérer le menu de l'admin
add_action('admin_menu', 'simplon_menu');
//hook qui se déclenche à l'ouverture du menu admin
add_action('admin_init', 'register_options');


/**
 * Fonction ajoutant un nouveau menu à l'administration du wordpress
 */
function simplon_menu() {
    add_menu_page('Simplon Plugin', 'Simplon', 'manage_options', 'simplon_menu', 'simplon_admin');
}
/**
 * Fonction qui crée un nouveau setting
 */
function register_options() {
    register_setting('simploption', 'admin-bg');
}
/**
 * Méthode qui génère l'affichage de la page d'administration 
 */
function simplon_admin() {
    echo '<h1>Simplon Admin</h1>';

    echo '<form method="post" action="options.php">';
    settings_fields( 'simploption' );
    do_settings_sections( 'simploption' );
    echo '<label>Couleur du header de l\'admin :</label> ';
    echo '<input type="color" name="admin-bg" value="' . get_option('admin-bg') . '" />';

    submit_button();
    echo '</form>';
}


/**
 * hook qui se déclenche à l'initialization du head des pages de l'administration
 */
add_action('admin_head', 'simplon_plugin_style');
add_action('wp_head', 'simplon_plugin_style'); //(pour que la couleur s'affiche aussi quand on est loggé sur une page du site)

function simplon_plugin_style() {
    ?>
<style>
    #wpadminbar, #adminmenu {
        
        background-color: <?php echo get_option('admin-bg') ?>;
    }

</style>

    <?php
}

add_action('widgets_init', 'widgets_register');

function widgets_register() {
    include_once __DIR__.'/FirstWidget.php';
    register_widget('FirstWidget');

    include_once __DIR__.'/ModalWidget.php';
    register_widget('ModalWidget');
}