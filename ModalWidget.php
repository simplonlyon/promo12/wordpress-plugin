<?php

use Timber\Timber;

class ModalWidget extends WP_Widget {

    public function __construct() {
        parent::__construct('modal_widget', 'Modal Widget', [
            'Un widget permettant d\'afficher une fenêtre modale'
        ]);
    }

    public function widget($args, $instance) {
        $context = Timber::context();
        $context['instance'] = $instance;

        Timber::render('modal.html.twig', $context);
    }

    public function form($instance)
    {
        $context = Timber::context();
        $context['instance'] = $instance;
        $context['content'] = [
            'id' => $this->get_field_id('content'),
            'name' => $this->get_field_name('content')
        ];

        Timber::render('admin-modal.html.twig', $context);
    }

    public function update($new_instance, $old_instance)
    {
        $instance = [];
        $instance['content'] = $new_instance['content'];
        return $instance;
    }
}